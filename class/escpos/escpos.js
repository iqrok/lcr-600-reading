const binNum = require('@iqrok/binnum');
const __serial = require('@iqrok/serial.helper');

class __escpos{
	constructor(config, debug){
		const self = this;
		self.config = config;
		self.debug = debug;
		self.CONSTANTS = require('./constant');
		self._message = [];
		self._mode = 0;
	};

	/**
	 *	Connect to serial port and add event listeners
	 * */
	async connect(){
		const self = this;

		self.serial = new __serial({
				...self.config,
				autoreconnect: false,
				autoopen: false,
				parser: {
					type: 'InterByteTimeout',
					interval: 30,
				}
			}, true);

		self.serial.on('open', received => {
			console.log('open', received);
		});

		self.serial.on('error', received => {
			console.error('error', received);
		});

		return self.serial
			.connect()
			.catch(error => {
				console.error(error);
			});
	}

	_appendBytes(arrBytes){
		const self = this;
		self._message.push(Buffer.from(arrBytes));
	};

	begin(){
		const self = this;
		self._message = [];
		self._mode = 0;

		self._appendBytes(self.CONSTANTS.HW_INIT);

		return this;
	};

	text(str){
		const self = this;
		self._appendBytes(str);

		return this;
	};

	pad(str, max, character = ' ') {
		const self = this;
		str = str.toString();
		return str.length < max ? self.pad(character + str, max) : str;
	}


	bold(option = false){
		const self = this;
		self._mode = binNum(self._mode).bit(self.CONSTANTS.MODE_BIT.BOLD.bit).set(+option);

		return this;
	};

	underline(option = false){
		const self = this;
		self._mode = binNum(self._mode).bit(self.CONSTANTS.MODE_BIT.UNDERLINE.bit).set(+option);

		return this;
	};

	font(option = false){
		const self = this;

		switch(option.toUpperCase()){
			case 'A':
				option = 0x00;
				break;

			case 'B':
				option = 0x01;
				break;
		}

		self._mode = binNum(self._mode).bit(self.CONSTANTS.MODE_BIT.FONT.bit).set(+option);

		return this;
	};

	dblHeight(option = 'LEFT'){
		const self = this;
		self._mode = binNum(self._mode).bit(self.CONSTANTS.MODE_BIT.DBL_HEIGHT.bit).set(+option);

		return this;
	};

	dblWidth(option = 'LEFT'){
		const self = this;
		self._mode = binNum(self._mode).bit(self.CONSTANTS.MODE_BIT.DBL_WIDTH.bit).set(+option);

		return this;
	};

	setMode(options = [1, 0, 0, 1, 1, 1, 1, 1]){
		const self = this;

		self._appendBytes([
				...self.CONSTANTS.CMD_SET_PRINT_MODE,
				self._mode
			]);

		return this;
	};

	/**
	 *	size [A,B,FULL,PARTIAL]
	 * */
	cut(option = 'A'){
		const self = this;
		option = option.toUpperCase();

		const _PAPER_CUT_SIZE = self.CONSTANTS.PAPER_CUT_SIZE[option] !== undefined
			? self.CONSTANTS.PAPER_CUT_SIZE[option]
			: self.CONSTANTS.PAPER_CUT_SIZE.A;

		self._appendBytes([
				...self.CONSTANTS.CMD_PAPER_CUT,
				_PAPER_CUT_SIZE
			]);

		return this;
	};

	release(str){
		const self = this;
		self._appendBytes(self.CONSTANTS.RELEASE);

		return this;
	};

	async print(){
		const self = this;

		if(!self.serial.isOpen){
			console.error('Printer is not open');
			return;
		}

		self.setMode();
		const packet = Buffer.concat([...self._message]);
		self._message = [];
		self._mode = 0;

		await self.serial.write(packet);

		return packet;
	};
};

module.exports = __escpos
