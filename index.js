global.__basedir = process.env.CWD || process.cwd();

const date = require('date-and-time');

const config = require(`${__basedir}/config/app.config`);
const helper = require(`${__basedir}/class/helper.class`);

const lcr600 = require('lcr600');
const LCR600 = new lcr600(config.lcr600, config.debug);

const reading = {
		async resetLCR(){
			console.log('STOPPING');
			await LCR600.issueCommand('STOP');
			console.log('RUNNING');
			await LCR600.issueCommand('RUN');
		},

		setDecimals(decimalPlace = 0){
			this._divider = 1 / (10 ** decimalPlace);
			return this._divider;
		},

		normalize(number){
			return number * (this._divider == undefined ? 1 : this._divider);
		},

		attrs: [
			'NoFlowTimer_DL',
			'Decimals_WM',
			'QtyUnits_WM',
			'UnitID_UL',
			'MeterID_WM',
			'Printer_WM',
			'SerialID_FL',
		],

		isStarted : false,
		sendCommand : true,
		lastIssuedCommand: undefined,
	};

// use setTimeout instead of setInterval due to async
const __loop = async () => {
		const interval = config.interval || 100;

		if(reading.sendCommand){
			const request = await LCR600.getData('GrossCount_NE');
		}

		setTimeout(__loop, interval);
	};

// listener for each summary
LCR600.on('GrossCount_NE', async received => {
	// meter is reset, go no further
	if(received.value.meter.finish === 0 || received.value.meter.finish === received.value.meter.start){
		return;
	}

	for(const key in received.value.meter){
		received.value.meter[key] = reading.normalize(received.value.meter[key]);
	}

	const data = {
			...received,
			attributes: LCR600.getAttribute(),
		};

	console.log('==================== SUMMARY ====================');
	console.dir(data, {depth:4});
	console.log('================ END OF SUMMARY =================');

	reading.sendCommand = false;

	console.log('STOPPING');
	await LCR600.issueCommand('STOP');
	console.log('PRINTING');
	await LCR600.issueCommand('PRINT');

	await helper.sleep(5000);

	console.log('STOPPING');
	await LCR600.issueCommand('STOP');

	reading.isStarted = false;
	reading.sendCommand = true;
});

// successfully parsing data
LCR600.on('data', (received) => {
	//~ console.log('data:');
	//~ console.dir(received, {depth:5});
	//~ console.log('---------------------------------------');
});

// failed when parsing data, only contains code for debugging purpose
LCR600.on('failed', received => {
	console.error('failed:', received);
	console.log('---------------------------------------');
});

// Listener when port is opened
LCR600.on('open', received => {
	console.log('open:', received);
	console.log('---------------------------------------');
});

// Listener when port is closed
LCR600.on('close', received => {
	console.log('close:', received);
	console.log('---------------------------------------');
});

// Listener when port is error
LCR600.on('error', received => {
	console.error('error:', received);
	console.log('---------------------------------------');
});

// listen to switch pos change
LCR600.on('switch', async received => {
	if(received.from && received.to){
		if(received.from.description === 'RUN' && received.to.description === 'STOP'){
			await LCR600.interruptSummary('GrossCount_NE');
		}

		if(received.from.description === 'STOP' && received.to.description === 'RUN'){
			reading.sendCommand = false;
			await reading.resetLCR();

			const reset = await LCR600.resetSummary('GrossCount_NE');
			reading.sendCommand = true;
		}

		return;
	}

	if(received.issued){
		const {issued} = received;

		if(issued.description === 'RUN'){
			if(reading.lastIssuedCommand && reading.lastIssuedCommand.description != 'RUN'){
				reading.sendCommand = false;
				await reading.resetLCR();
				reading.sendCommand = true;
			}

			reading.isStarted = true;
		} else {
			reading.isStarted = false;
		}

		reading.lastIssuedCommand = issued;
	}

	console.log('switch change:', received);
	console.log('---------------------------------------');
});

(async() => {
	// connect to Serialport
	await LCR600.connect();

	// get Product Id and Set synchronization
	await LCR600.getProductID(true);

	// Set attributes data from LCR600
	for(const attribute of reading.attrs){
		await LCR600.requestAttribute(attribute);
	}

	reading.setDecimals(LCR600.getAttribute('Decimals_WM'));

	console.log('List set attributes:', LCR600.getAttribute());
	console.log('---------------------------------------');

	// set waiting for flow to stop within 60 seconds
	LCR600.setWaitingTime(60000);

	await LCR600.getDeviceStatus();

	await reading.resetLCR();
	console.log('STOPPING');
	await LCR600.issueCommand('STOP');

	reading.isStarted = false;

	// start infinite loop
	__loop();
})();
