module.exports = {
	lcr600: {
		hostAddress: 0xff,
		LCRNodeAddress: 0x01,
		port: "/dev/ttyUSB0",
		baud: 19200,
		parser: {
			type: 'InterByteTimeout',
			interval: 30,
		},
	},

	interval: 100,

	debug: false,
};
